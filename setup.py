#!/usr/bin/env python3

from distutils.core import setup

setup(name="octoprogress",
        version="1.0",
        description="Simple status display for octoprint",
        author="Alice",
        author_email="alice@dn3s.me",
        packages=["octoprogress"],
        entry_points={
            "console_scripts": [
                "octoprogress = octoprogress:run"
                ]
            }
        )
