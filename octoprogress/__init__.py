#!/usr/bin/env python3
from progress.bar import IncrementalBar as Bar
import os
import appdirs
import websocket
import json
import requests
import urllib.parse
import contextlib

def login(url, apikey):
    r = requests.post(url.rstrip("/")+"/api/login", data={"passive": "true", "apikey": apikey})
    if r.status_code != 200:
        r.raise_for_status()
    j = r.json()
    return j["name"] + ":" + j["session"]

@contextlib.contextmanager
def socket(url, apikey):
    parsed = urllib.parse.urlparse(url)
    parsed = tuple([{"http": "ws", "https": "wss"}[parsed.scheme], *parsed[1:]])
    conn = websocket.create_connection(urllib.parse.urlunparse(parsed).rstrip("/") + "/sockjs/websocket")
    try:
        if json.loads(conn.recv()).get("connected") is None:
            raise ValueError("Expected Connected message")
        conn.send(json.dumps({"auth": login(url, apikey)}))
        yield conn
    finally:
        conn.close()

def getState(conn):
    while True:
        message = json.loads(conn.recv())
        body = message.get("current") or message.get("history")
        if body is not None:
            return body

def getConfig():
    with open(os.path.join(appdirs.user_config_dir("octoprogress"), "config.json")) as f:
        data = json.load(f)
        return data["url"], data["token"]

tempsCache = {}
def run():
    try:
        url, token = getConfig()
        with socket(url, token) as conn:
            with Bar("", max=10000, suffix="%(percent)d%%") as bar:
                while True:
                    state = getState(conn)
                    bar.index = (state["progress"]["completion"] or 0)*100
                    bar.message = state["state"]["text"]
                    bar.suffix = "%(percent)d%%"
                    # ETA Label
                    timeLeft = state["progress"]["printTimeLeft"]
                    if timeLeft:
                        hours, r = divmod(timeLeft, 3600)
                        minutes, seconds = divmod(r, 60)
                        bar.suffix += f" ETA: {hours}:{minutes:02}:{seconds:02}"
                    # Temps Labels
                    if len(state.get("temps", [])):
                        temps = sorted(state["temps"], key=lambda i: i["time"])[-1]
                        for label, temp in sorted(temps.items()):
                            if label != "time" and temp["actual"] is not None and temp["target"] is not None:
                                if label not in tempsCache:
                                    tempsCache[label] = {}
                                tempsCache[label]["actual"] = temp["actual"]
                                tempsCache[label]["target"] = temp["target"]
                    for key, temp in sorted(tempsCache.items()):
                        label = key
                        bar.suffix += f" {label.title()}: {temp['actual']:.0f}°"
                        delta = abs(temp["actual"]-temp["target"])
                        bestDelta = abs(temp.get("best", 0)-temp['target'])
                        if delta < bestDelta:
                            tempsCache[key]["best"] = temp["actual"]
                        if bestDelta >1:
                            bar.suffix += f"/{temp['target']:.0f}°"
                    # Z labels
                    if state.get('currentZ') is not None:
                        bar.suffix += f" Z: {state['currentZ']:.1f}mm"
                    bar.update()
    except KeyboardInterrupt:
        pass
