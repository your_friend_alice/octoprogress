# octoprogress

Simple status display for octoprint. Shows status and print progress, with a visual progress bar.

## Installation

```
pip3 install .
```

## Config File:

The config file is a simple JSON object with the following entries. On linux, this file is located at `~/.config/octoprogress/config.json`:

| Key | Type | Description |
|-----|------|-------------|
| `url` | string | Base URL to your octoprint server |
| `token` | string | Octoprint API token |
